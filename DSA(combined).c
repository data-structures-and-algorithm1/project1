#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Defining the structure for student information
struct Student {
    char name[51]; // The name characters must not exceed 50
    char dob[11];  // 10 character field of format YYYY-MM-DD
    char RegNO[7]; // 6 character field for registration number padded with zeros
    char program_code[5];  // 4 character field for program code being taken by a student
    float Annual_Tuition; 
};

// Function to add a new student
void addStudent(struct Student students[], int* numStudents) {
    if (*numStudents >= 100) {
        printf("Error: Maximum number of students reached.\n");
        return;
    }
    struct Student newStudent;
    printf("Enter student name: ");
    scanf("%50s", newStudent.name);
    printf("Enter the student date of birth: ");
    scanf("%10s", newStudent.dob);
    printf("Enter student registration number: ");
    scanf("%6s", newStudent.RegNO);
    printf("Enter the student's program code: ");
    scanf("%4s", newStudent.program_code);
    printf("Enter student's annual tuition: ");
    scanf("%f", &newStudent.Annual_Tuition);

    students[*numStudents] = newStudent;
    (*numStudents)++;
    printf("Student added successfully!\n");
}

// Function to display all students
void displayStudents(const struct Student students[], int numStudents) {
    if (numStudents == 0) {
        printf("There are no students to display yet!\n");
        return;
    }

    printf("Student List:\n");
    int i;
    for (i = 0; i < numStudents; i++) {
        printf("Name: %s, Date of birth: %s, Registration Number: %s, Program code: %s, Annual Tuition: %.2f\n", students[i].name, students[i].dob, students[i].RegNO, students[i].program_code, students[i].Annual_Tuition);
    }
}

// Function to update a student's information
void updateStudent(struct Student students[], int numStudents) {
    char searchRegNO[7];
    printf("Enter registration number to update: ");
    scanf("%6s", searchRegNO);
    int i;
    for (i = 0; i < numStudents; i++) {
        if (strcmp(students[i].RegNO, searchRegNO) == 0) {
            printf("Enter new student name: ");
            scanf("%50s", students[i].name);
            printf("Enter new student date of birth: ");
            scanf("%10s", students[i].dob);
            printf("Enter new student registration number: ");
            scanf("%6s", students[i].RegNO);
            printf("Enter new student's program code: ");
            scanf("%4s", students[i].program_code);
            printf("Enter new student's annual tuition: ");
            scanf("%f", &students[i].Annual_Tuition);
            printf("Student record updated successfully!\n");
            return;
        }
    }
    printf("Student with registration number %s not found.\n", searchRegNO);
}

// Function to delete a student's record
void deleteStudent(struct Student students[], int* numStudents) {
    char searchRegNO[7];
    printf("Enter registration number to delete: ");
    scanf("%6s", searchRegNO);
    int i, j;
    for (i = 0; i < *numStudents; i++) {
        if (strcmp(students[i].RegNO, searchRegNO) == 0) {
            for (j = i; j < *numStudents - 1; j++) {
                students[j] = students[j + 1];
            }
            (*numStudents)--;
            printf("Student record deleted successfully!\n");
            return;
        }
    }
    printf("Student with registration number %s not found.\n", searchRegNO);
}

// Function to search for a student by their Registration number
void searchStudentByRegNo(const struct Student students[], int numStudents) {
    char searchRegNO[7];
    printf("Enter registration number to search: ");
    scanf("%6s", searchRegNO);
    int i;
    for (i = 0; i < numStudents; i++) {
        if (strcmp(students[i].RegNO, searchRegNO) == 0) {
            printf("Student found:\n");
            printf("Name: %s, Date of birth: %s, Registration Number: %s, Program code: %s, Annual Tuition: %.2f\n", students[i].name, students[i].dob, students[i].RegNO, students[i].program_code, students[i].Annual_Tuition);
            return;
        }
    }
    printf("Student with registration number %s not found.\n", searchRegNO);
}

// Function to sort the student list based on the chosen field
int compareByName(const void* a, const void* b);
int compareByRegNo(const void* a, const void* b);
void sortStudents(struct Student students[], int numStudents, int sortBy) {
    switch (sortBy) {
        case 1:
            qsort(students, numStudents, sizeof(struct Student), compareByName);
            break;
        case 2:
            qsort(students, numStudents, sizeof(struct Student), compareByRegNo);
            break;
        default:
            printf("Invalid option for sorting.\n");
    }
}

// Comparison function for sorting by name
int compareByName(const void* a, const void* b) {
    return strcmp(((struct Student*)a)->name, ((struct Student*)b)->name);
}

// Comparison function for sorting by registration number
int compareByRegNo(const void* a, const void* b) {
    return strcmp(((struct Student*)a)->RegNO, ((struct Student*)b)->RegNO);
}

// Function to write records to the file
void export_records_to_file(const struct Student students[], int numStudents) {
    FILE *fp;
    fp = fopen("student_records.csv", "a");

    if (fp == NULL) {
        printf("Error opening file.\n");
        return;
    }

    if (ftell(fp) == 0) {
        fprintf(fp, "Name,Date of Birth,Registration Number,Program Code,Annual Tuition\n");
    }

    int i; // Move the declaration of i outside the loop header
    for (i = 0; i < numStudents; i++) {
        fprintf(fp, "%s,%s,%s,%s,%.2f\n", students[i].name, students[i].dob, students[i].RegNO, students[i].program_code, students[i].Annual_Tuition);
    }

    fclose(fp);
    printf("Records exported to 'student_records.csv' successfully.\n");
}

int main() {
    struct Student studentArray[100];
    int numStudents = 0;

    int choice;
    do {
        printf("\nMenu:\n");
        printf("1. Add Student\n");
        printf("2. Display all Students\n");
        printf("3. Update Student\n");
        printf("4. Delete Student\n");
        printf("5. Search Student by Registration Number\n");
        printf("6. Sort by either name or registration number\n");
        printf("7. Send the records to the file\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                addStudent(studentArray, &numStudents);
                break;
            case 2:
                displayStudents(studentArray, numStudents);
                break;
            case 3:
                updateStudent(studentArray, numStudents);
                break;
            case 4:
                deleteStudent(studentArray, &numStudents);
                break;
            case 5:
                searchStudentByRegNo(studentArray, numStudents);
                break;
            case 6:
                printf("Sort by (1) Name or (2) Registration Number: ");
                int sortBy;
                scanf("%d", &sortBy);
                sortStudents(studentArray, numStudents, sortBy);
                break;
            case 7:
                export_records_to_file(studentArray, numStudents);
                break;
            case 8:
                printf("Exiting program. Goodbye!\n");
                break;
            default:
                printf("Invalid choice. Please try again.\n");
        }
    } while (choice != 8);

    return 0;
}
