

## Students' Register

This system enables an administrator to encapsulate and manage students' details enrolled in the school such as students' name, registration number, date of birth, program code of the course a student is taking and annual tuition of each student.

The system built based on C programing language and due to the need to capture fixed size and contiguous memory, enable fast and efficient access, we have used arrays of structures to implement the system.

## Contributors
| Name  | Contact |
| ---   | --------|
|Kipaalu Derrick |   kipaaluderrick@gmail.com |
|Ssebaleke John |   @jsvico |
|Jonathan Mukalazi | jonbenoni@gmail.com |
|Martha Hasahya |@marthahasahya256|
|Ssozi Henry | ssozihenry123@gmail.com |

## video links
Martha Hasahya: code explanation for searching a student by their Registration number (part c)
https://youtu.be/YfSysUoKoHE