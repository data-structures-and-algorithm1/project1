#include <stdio.h>
#include <stdlib.h>

void export_records_to_file() {
    FILE *fp;
    fp = fopen("student_records.csv", "a"); // Open the file in append mode

    if (fp == NULL) {
        printf("Error opening file.\n");
        return;
    }

    // Write header if the file is empty
    if (ftell(fp) == 0) {
        fprintf(fp, "Name,Date of Birth,Registration Number,Program Code,Annual Tuition\n");
    }

    // Write each student record to the file
    for (int i = 0; i < num_students; i++) {
        fprintf(fp, "%s,%s,%s,%s,%.2f\n", students[i].name, students[i].dob, students[i].registration, students[i].program_code, students[i].annual_tuition);
    }

    fclose(fp);
    printf("Records exported to 'student_records.csv' successfully.\n");
}