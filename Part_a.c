struct Student {
  char name[51]; // 50 characters for name, plus 1 for null-terminator
  char dob[11]; // standard date format YYYY-MM-DD, 10 characters, plus 1 for null-terminator
  char regNO[7]; // 6 numerical characters, plus 1 for null-terminator
  char program_code[5]; // 4 characters for program code, plus 1 for null-terminator
  float Annual_tuition; // annual tuition, as a float
};

